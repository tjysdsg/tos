#ifndef TOS_KERNEL_PS2_KEYBOARD_H
#define TOS_KERNEL_PS2_KEYBOARD_H

#ifdef __cplusplus
extern "C" {
#endif

void init_ps2_keyboard();

#ifdef __cplusplus
}
#endif

#endif //TOS_KERNEL_PS2_KEYBOARD_H

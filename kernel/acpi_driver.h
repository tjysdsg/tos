#ifndef TOS_KERNEL_ACPI_DRIVER_H
#define TOS_KERNEL_ACPI_DRIVER_H

#ifdef __cplusplus
extern "C" {
#endif

void init_acpi();

#ifdef __cplusplus
}
#endif

#endif //TOS_KERNEL_ACPI_DRIVER_H

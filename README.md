# TOS

*WIP*

An x86 hobby OS in C++

- Multiboot: https://www.gnu.org/software/grub/manual/multiboot/multiboot.html
- APIC and IOAPIC
- Paging
- Implements libc
- VGA or VBE console
- PS/2 keyboard
- ...

# TODO

- Paging with PAE
- Multi-processor
- Process
- Userland
